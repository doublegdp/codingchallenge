# Coding Challenge Submission
This repo has 2 coding challenges in PDF format(One for Rails and one for Reactjs).   As a candidate, you must pick the challenge for which you are interviewing for.  
Follow the instructions and if they are unclear, please contact the hiring manager. 

Thank you very much for applying at DoubleGDP.

# Important!!
Do not submit a pull request to this repository.  You PR wil be rejected and your submission ignored.
To properly submit a coding challenge you must:

- fork this repository
- make the necessary changes
- push changes to your forked origin
- send address of your fork to the hiring manager.
- Give read access to the following reviewers:  
    - nicolas@doublegdp.com  
    - nurudeen@doublegdp.com  
    - saurabh@doublegdp.com  
    - olivier@doublegdp.com  

We will review your fork online before and during your interview.







